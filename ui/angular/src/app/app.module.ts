import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LoginHomeComponent } from './login/home.component';
import { LoginOtpComponent } from './login/otp.component';
import { LoginRecoveryComponent } from './login/recovery.component';
import { UserComponent } from './user/user.component';
import { UserHomeComponent } from './user/home.component';
import { UserOtpComponent } from './user/otp.component';

import { AuthService } from './_services/auth.service';

const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    children: [
      { path: '', component: LoginHomeComponent },
      { path: 'otp', component: LoginOtpComponent },
      { path: 'recovery', component: LoginRecoveryComponent }
    ]
  },
  {
    path: 'user',
    component: UserComponent,
    children: [
      { path: '', component: UserHomeComponent },
      { path: 'otp', component: UserOtpComponent }
    ]
  },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginHomeComponent,
    LoginOtpComponent,
    LoginRecoveryComponent,
    UserComponent,
    UserHomeComponent,
    UserOtpComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    )
  ],
  providers: [
    AuthService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
