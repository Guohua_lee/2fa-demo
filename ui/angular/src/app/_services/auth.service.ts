export class AuthService {
    private sessionKey = '';
    private loggedIn = false;

    isAuthenticated() {
        return this.sessionKey.length > 0;
    }

    isLoggedIn() {
        return this.loggedIn;
    }

    setLoggedIn(state: boolean) {
        this.loggedIn = state;
    }

    getSessionKey() {
        return this.sessionKey;
    }

    setSessionKey(key: string) {
        this.sessionKey = key;
    }

    getAuthorisationHeader() {
        return 'Basic ' + this.getSessionKey();
    }

    logout() {
        this.sessionKey = '';
        this.loggedIn = false;
    }
}
