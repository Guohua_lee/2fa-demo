import { AuthService } from '../_services/auth.service';
import { Component, Input, OnInit } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  templateUrl: './otp.component.html'
})
export class LoginOtpComponent implements OnInit {
  @Input() otp: string;

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    /*if (!this.authService.isAuthenticated()) {
      this.router.navigate(['/login']);
    }
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['/user']);
    }*/
  }

  onLoginOtpFormSubmit(): void {
    const payload = {sessionKey: this.authService.getSessionKey(), otp: this.otp.toString()};
    this.httpClient
      .post(environment.serverUri + '/login/otp', payload)
      .subscribe((resp: any) => {
        this.authService.setLoggedIn(true);
        this.router.navigate(['/user']);
      }, error => {
        alert('Otp validation failed, please try again');
        this.router.navigate(['/login']);
      });
  }
}
