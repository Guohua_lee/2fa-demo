import { AuthService } from '../_services/auth.service';
import { Component, Input, OnInit } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  templateUrl: './home.component.html'
})
export class LoginHomeComponent implements OnInit {
  @Input() loginModel: object = {};

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.authService.logout();
  }

  onLoginFormSubmit() {
    this.httpClient
      .post(environment.serverUri + '/login', this.loginModel)
      .subscribe((resp: any) => {
        this.authService.setSessionKey(resp.sessionKey);
        this.authService.setLoggedIn(!resp.require2fa);
        if (!this.authService.isLoggedIn()) {
          this.router.navigate(['/login/otp']);
        } else {
          this.router.navigate(['/user']);
        }
      }, error => {
        alert('Login failed, please try again');
      });
  }
}
