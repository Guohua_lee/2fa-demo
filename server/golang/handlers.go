package main

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"reflect"
	"time"

	"github.com/pquerna/otp/totp"
	"github.com/satori/go.uuid"

	_ "github.com/lib/pq"
)

type user struct {
	UserID       uuid.UUID
	Username     string
	PasswordSalt []byte
	PasswordHash []byte
	OtpEnabled   bool
}

type loginRequest struct {
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

type loginResponse struct {
	Require2FA bool   `json:"require2fa"`
	SessionKey string `json:"sessionKey,omitempty"`
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	var err error

	// parse result
	var lreq loginRequest
	if err = json.NewDecoder(r.Body).Decode(&lreq); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("unable to parse json: " + err.Error()))
		return
	}

	// find user
	var rows *sql.Rows
	if rows, err = db.Query("SELECT * FROM users WHERE username = $1", lreq.Username); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("unable to get user: " + err.Error()))
		return
	}
	defer rows.Close()

	var u user
	for rows.Next() {
		var uidStr string
		rows.Scan(&uidStr, &u.Username, &u.PasswordSalt, &u.PasswordHash, &u.OtpEnabled)

		var uid uuid.UUID
		if uid, err = uuid.FromString(uidStr); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("error parsing uuid: " + err.Error()))
			return
		}
		u.UserID = uid
	}
	if uuid.Equal(u.UserID, uuid.Nil) {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// verify password
	if cHash, err := getPasswordHash([]byte(lreq.Password), u.PasswordSalt); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error getting secure hash: " + err.Error()))
		return
	} else if !reflect.DeepEqual(cHash, u.PasswordHash) {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// create session key
	var skey string
	if skey, err = getRandomHexString(32); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error getting session key: " + err.Error()))
		return
	}

	pwdExpire := time.Now().UTC().Add(1 * time.Hour).Format(time.RFC3339Nano)
	tfaExpire := time.Now().UTC().Add(3560 * 24 * time.Hour).Format(time.RFC3339Nano)
	sessionQuery := "INSERT INTO sessions VALUES ($1, $2, $3, $4, $5, $6)"
	if _, err = db.Exec(sessionQuery, uuid.NewV4(), u.UserID, skey, pwdExpire, tfaExpire, !u.OtpEnabled); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error creating session: " + err.Error()))
		return
	}

	// response
	lrsp := loginResponse{Require2FA: u.OtpEnabled, SessionKey: skey}
	if lrsp.Require2FA {
		w.WriteHeader(http.StatusAccepted)
	} else {
		w.WriteHeader(http.StatusOK)
	}

	w.Header().Set("Content-Type", "application/json")
	body, _ := json.Marshal(lrsp)
	w.Write(body)
}

type loginOtpRequest struct {
	SessionKey   string `json:"sessionKey,omitempty"`
	OneTimeToken string `json:"otp,omitempty"`
}

type loginOtpResponse struct {
	SessionKey string `json:"sessionKey,omitempty"`
}

func loginOtpHandler(w http.ResponseWriter, r *http.Request) {
	var err error

	// parse result
	var loreq loginOtpRequest
	if err = json.NewDecoder(r.Body).Decode(&loreq); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("unable to parse json: " + err.Error()))
		return
	}

	// get session info and otp secret
	var rows *sql.Rows
	if rows, err = db.Query(
		`SELECT o.secret
			FROM sessions AS s LEFT JOIN otp AS o ON s.user_id = o.user_id
			WHERE s.session_key = $1`, loreq.SessionKey); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("unable to get session: " + err.Error()))
		return
	}
	defer rows.Close()

	var secret string
	for rows.Next() {
		rows.Scan(&secret)
	}
	if secret == "" {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	// validate
	if !totp.Validate(loreq.OneTimeToken, secret) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	// update session
	sessionQuery :=
		`UPDATE sessions
			SET password_auth_expires_at = $1, two_factor_auth_expires_at = $2, fully_logged_in = $3
			WHERE session_key = $4`
	pwdExpire := time.Now().UTC().Add(1 * time.Hour).Format(time.RFC3339Nano)
	tfaExpire := time.Now().UTC().Add(24 * time.Hour).Format(time.RFC3339Nano)
	if _, err = db.Exec(sessionQuery, pwdExpire, tfaExpire, true, loreq.SessionKey); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error updating session: " + err.Error()))
		return
	}

	// send response
	w.WriteHeader(http.StatusOK)
	loresp := loginOtpResponse{SessionKey: loreq.SessionKey}
	body, _ := json.Marshal(loresp)
	w.Header().Set("Content-Type", "application/json")
	w.Write(body)
}

type changePasswordRequest struct {
	SessionKey      string `json:"sessionKey,omitempty"`
	CurrentPassword string `json:"cPassword,omitempty"`
	NewPassword     string `json:"nPassword,omitempty"`
}

func changePasswordHandler(w http.ResponseWriter, r *http.Request) {
	var err error

	// parse result
	var cpreq changePasswordRequest
	if err = json.NewDecoder(r.Body).Decode(&cpreq); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("unable to parse json: " + err.Error()))
		return
	}

	// validate session
	var u user
	if u, err = validateAndRenewSession(cpreq.SessionKey); err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("unable to authenticate, session may have expired: " + err.Error()))
		return
	}

	// verify password
	if cHash, err := getPasswordHash([]byte(cpreq.CurrentPassword), u.PasswordSalt); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error getting secure hash: " + err.Error()))
		return
	} else if !reflect.DeepEqual(cHash, u.PasswordHash) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	// update password
	var nHash []byte
	nSalt, _ := getRandomBytes(32)
	if nHash, err = getPasswordHash([]byte(cpreq.NewPassword), nSalt); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error getting secure hash: " + err.Error()))
		return
	}

	updatePasswordQuery := "UPDATE users SET password_salt = $1, password_hash = $2 WHERE user_id = $3"
	if _, err = db.Exec(updatePasswordQuery, nSalt, nHash, u.UserID); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error updating password: " + err.Error()))
		return
	}

	// response
	w.WriteHeader(http.StatusAccepted)
}
