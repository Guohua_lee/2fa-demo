package main

import (
	"database/sql"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/urfave/negroni"
)

const dbConStr = "postgres://postgres:dbP%40ssw0rd123@localhost/postgres?sslmode=disable"
const listenAddr = "127.0.0.1:8080"

var db *sql.DB

func main() {
	var err error

	// set up database
	if db, err = sql.Open("postgres", dbConStr); err != nil {
		log.Fatal(err)
	}

	// set up http server
	r := mux.NewRouter()
	r.HandleFunc("/login", loginHandler).Methods("POST")
	r.HandleFunc("/login/otp", loginOtpHandler).Methods("POST")
	r.HandleFunc("/user/password", changePasswordHandler).Methods("PUT")

	n := negroni.New()
	n.Use(negroni.NewRecovery())
	n.Use(negroni.NewLogger())
	n.Use(cors.Default())
	n.UseHandler(r)

	srv := &http.Server{
		Addr:           listenAddr,
		Handler:        n,
		ReadTimeout:    30 * time.Second,
		WriteTimeout:   30 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(srv.ListenAndServe())
}
