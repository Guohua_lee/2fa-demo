package main

import (
	"database/sql"
	"errors"
	"time"

	uuid "github.com/satori/go.uuid"

	_ "github.com/lib/pq"
)

func validateAndRenewSession(sessionKey string) (user, error) {
	var err error
	var u user

	// get user and validate session
	var rows *sql.Rows
	if rows, err = db.Query(
		`SELECT s.password_auth_expires_at, s.two_factor_auth_expires_at, s.fully_logged_in, u.*
			FROM sessions AS s LEFT JOIN users AS u ON s.user_id = u.user_id
			WHERE s.session_key = $1`, sessionKey); err != nil {
		return u, err
	}
	defer rows.Close()

	for rows.Next() {
		var pExpire, tfaExpire time.Time
		var loggedIn bool
		var uidStr string
		rows.Scan(&pExpire, &tfaExpire, &loggedIn,
			&uidStr, &u.Username, &u.PasswordSalt, &u.PasswordHash, &u.OtpEnabled)

		if pExpire.Before(time.Now().UTC()) ||
			tfaExpire.Before(time.Now().UTC()) ||
			loggedIn == false ||
			uidStr == "" {

			return u, errors.New("user session has expired")
		}

		var uid uuid.UUID
		if uid, err = uuid.FromString(uidStr); err != nil {
			return u, err
		}
		u.UserID = uid
	}

	if u.Username == "" {
		return u, errors.New("unable to find valid user")
	}

	// update session
	sessionQuery := `UPDATE sessions SET password_auth_expires_at = $1 WHERE session_key = $2`
	pwdExpire := time.Now().UTC().Add(1 * time.Hour).Format(time.RFC3339Nano)
	if _, err = db.Exec(sessionQuery, pwdExpire, sessionKey); err != nil {
		return u, err
	}

	return u, nil
}
