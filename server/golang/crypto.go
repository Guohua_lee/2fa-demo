package main

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"

	"golang.org/x/crypto/scrypt"
)

func getRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func getRandomHexString(s int) (string, error) {
	b, err := getRandomBytes(s)
	return hex.EncodeToString(b), err
}

func getRandomBase64String(s int) (string, error) {
	b, err := getRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b), err
}

func getPasswordHash(password, salt []byte) ([]byte, error) {
	return scrypt.Key(password, salt, 1<<15, 8, 1, 32)
}
