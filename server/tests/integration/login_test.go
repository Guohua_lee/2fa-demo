package main

import (
	"bytes"
	"fmt"
	"net/http"
	"regexp"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestLogin(t *testing.T) {

	var err error
	var resp *http.Response
	var lresp loginResponse

	hexRegex := regexp.MustCompile("[0-9a-f]+")
	loginURL := serverURL + "/login"
	invalidUser1 := []byte(`{"username":"user3","password":"password1"}`)
	incorrectUser1 := []byte(`{"username":"user2","password":"password2"}`)
	userNo2Fa := []byte(`{"username":"user1","password":"password1"}`)
	userWith2Fa := []byte(`{"username":"user2","password":"password1"}`)

	t.Run("UserNotFound", func(t *testing.T) {
		if resp, err = http.Post(loginURL, "application/json", bytes.NewBuffer(invalidUser1)); err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusUnauthorized, resp.StatusCode, "should return 401")
	})

	t.Run("IncorrectPassword", func(t *testing.T) {
		if resp, err = http.Post(loginURL, "application/json", bytes.NewBuffer(incorrectUser1)); err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusUnauthorized, resp.StatusCode, "should return 401")
	})

	t.Run("SuccessNo2FA", func(t *testing.T) {
		if resp, err = http.Post(loginURL, "application/json", bytes.NewBuffer(userNo2Fa)); err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusOK, resp.StatusCode, "should return 200")

		if lresp, err = parseLoginResponse(resp); err != nil {
			t.Error(err)
		}
		assert.False(t, lresp.Require2FA, "require_2fa should be false")
		assert.True(t, hexRegex.MatchString(lresp.SessionKey), "session key should be valid")
	})

	t.Run("SuccessWith2FA", func(t *testing.T) {
		if resp, err = http.Post(loginURL, "application/json", bytes.NewBuffer(userWith2Fa)); err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusAccepted, resp.StatusCode, "should return 202")

		if lresp, err = parseLoginResponse(resp); err != nil {
			t.Error(err)
		}
		assert.True(t, lresp.Require2FA, "require_2fa should be true")
		assert.True(t, hexRegex.MatchString(lresp.SessionKey), "session key should be valid")
	})
}

func TestLoginOtp(t *testing.T) {

	var err error
	var resp *http.Response
	var lresp loginResponse

	hexRegex := regexp.MustCompile("[0-9a-f]+")
	loginURL := serverURL + "/login"
	loginOtpURL := serverURL + "/login/otp"

	// look for file user2-otp-qrcode to get the qr code on your phone
	userWith2Fa := []byte(`{"username":"user2","password":"password1"}`)

	// login
	if resp, err = http.Post(loginURL, "application/json", bytes.NewBuffer(userWith2Fa)); err != nil {
		t.Error(err)
	}
	assert.Equal(t, http.StatusAccepted, resp.StatusCode, "should return 202")

	if lresp, err = parseLoginResponse(resp); err != nil {
		t.Error(err)
	}
	assert.True(t, lresp.Require2FA, "require_2fa should be true")
	assert.True(t, hexRegex.MatchString(lresp.SessionKey), "session key should be valid")

	skey := lresp.SessionKey

	t.Run("NoOtpCode", func(t *testing.T) {
		loreq := []byte(fmt.Sprintf(`{"sessionKey":"%s","otp":"%s"}`, skey, ""))
		if resp, err = http.Post(loginOtpURL, "application/json", bytes.NewBuffer(loreq)); err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusForbidden, resp.StatusCode, "should return 403")
	})

	t.Run("CorrectOtpCode1MinEarlier", func(t *testing.T) {
		loreq := getLoginOtpPayload(skey, -60*time.Second)
		if resp, err = http.Post(loginOtpURL, "application/json", bytes.NewBuffer(loreq)); err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusForbidden, resp.StatusCode, "should return 403")
	})

	t.Run("CorrectOtpCode1MinLater", func(t *testing.T) {
		loreq := getLoginOtpPayload(skey, 60*time.Second)
		if resp, err = http.Post(loginOtpURL, "application/json", bytes.NewBuffer(loreq)); err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusForbidden, resp.StatusCode, "should return 403")
	})

	t.Run("CorrectOtpCode30SecEarlier", func(t *testing.T) {
		loreq := getLoginOtpPayload(skey, -30*time.Second)
		if resp, err = http.Post(loginOtpURL, "application/json", bytes.NewBuffer(loreq)); err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusOK, resp.StatusCode, "should return 200")
	})

	t.Run("CorrectOtpCode30SecLater", func(t *testing.T) {
		loreq := getLoginOtpPayload(skey, 30*time.Second)
		if resp, err = http.Post(loginOtpURL, "application/json", bytes.NewBuffer(loreq)); err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusOK, resp.StatusCode, "should return 200")
	})

	t.Run("CorrectOtpCodeCurrentTime", func(t *testing.T) {
		loreq := getLoginOtpPayload(skey, 0)
		if resp, err = http.Post(loginOtpURL, "application/json", bytes.NewBuffer(loreq)); err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusOK, resp.StatusCode, "should return 200")
	})
}
