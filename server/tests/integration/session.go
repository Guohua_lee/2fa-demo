package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"testing"
	"time"

	"github.com/pquerna/otp/totp"
	"github.com/stretchr/testify/assert"
)

type loginResponse struct {
	Require2FA bool   `json:"require2fa"`
	SessionKey string `json:"sessionKey,omitempty"`
}

func getLoginOtpPayload(skey string, td time.Duration) []byte {
	otpSecret := "TFH7JX2CQQ7XZIWX"
	code, _ := totp.GenerateCode(otpSecret, time.Now().UTC().Add(td))
	return []byte(fmt.Sprintf(`{"sessionKey":"%s","otp":"%s"}`, skey, code))
}

func parseLoginResponse(r *http.Response) (loginResponse, error) {
	var lresp loginResponse
	if err := json.NewDecoder(r.Body).Decode(&lresp); err != nil {
		return lresp, err
	}
	defer r.Body.Close()
	return lresp, nil
}

func getSessionKeyNo2Fa(t *testing.T) string {
	var err error
	var resp *http.Response
	var lresp loginResponse

	loginURL := serverURL + "/login"
	hexRegex := regexp.MustCompile("[0-9a-f]+")
	userNo2Fa := []byte(`{"username":"user1","password":"password1"}`)

	if resp, err = http.Post(loginURL, "application/json", bytes.NewBuffer(userNo2Fa)); err != nil {
		t.Error(err)
	}
	assert.Equal(t, http.StatusOK, resp.StatusCode, "should return 200")

	if lresp, err = parseLoginResponse(resp); err != nil {
		t.Error(err)
	}
	assert.False(t, lresp.Require2FA, "require_2fa should be false")
	assert.True(t, hexRegex.MatchString(lresp.SessionKey), "session key should be valid")

	return lresp.SessionKey
}

func getSessionKeyWith2Fa(t *testing.T) string {
	var err error
	var resp *http.Response
	var lresp loginResponse

	loginURL := serverURL + "/login"
	loginOtpURL := serverURL + "/login/otp"
	userWith2Fa := []byte(`{"username":"user2","password":"password1"}`)

	if resp, err = http.Post(loginURL, "application/json", bytes.NewBuffer(userWith2Fa)); err != nil {
		t.Error(err)
	}
	assert.Equal(t, http.StatusAccepted, resp.StatusCode, "should return 202")

	if lresp, err = parseLoginResponse(resp); err != nil {
		t.Error(err)
	}
	assert.True(t, lresp.Require2FA, "require_2fa should be true")

	skey := lresp.SessionKey

	loreq := getLoginOtpPayload(skey, 0)
	if resp, err = http.Post(loginOtpURL, "application/json", bytes.NewBuffer(loreq)); err != nil {
		t.Error(err)
	}
	assert.Equal(t, http.StatusOK, resp.StatusCode, "should return 200")

	return skey
}
