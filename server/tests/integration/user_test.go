package main

import (
	"bytes"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestChangePassword(t *testing.T) {

	var resp *http.Response
	client := http.Client{}

	changePasswordURL := serverURL + "/user/password"

	skey1 := getSessionKeyNo2Fa(t)
	skey2 := getSessionKeyWith2Fa(t)

	t.Run("InvalidCurrentPassword", func(t *testing.T) {
		payload := []byte(fmt.Sprintf(`{"sessionKey":"%s","cPassword":"%s","nPassword":"%s"}`, skey1, "password234", "password234"))
		client := http.Client{}
		req, err := http.NewRequest(http.MethodPut, changePasswordURL, bytes.NewBuffer(payload))
		if err != nil {
			t.Error(err)
		}
		resp, err = client.Do(req)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusForbidden, resp.StatusCode, "should return 403")
	})

	t.Run("ValidCurrentPasswordNo2FA", func(t *testing.T) {
		payload := []byte(fmt.Sprintf(`{"sessionKey":"%s","cPassword":"%s","nPassword":"%s"}`, skey1, "password1", "password2"))
		req, err := http.NewRequest(http.MethodPut, changePasswordURL, bytes.NewBuffer(payload))
		if err != nil {
			t.Error(err)
		}
		resp, err = client.Do(req)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusAccepted, resp.StatusCode, "should return 202")

		// chaneg it back
		payload = []byte(fmt.Sprintf(`{"sessionKey":"%s","cPassword":"%s","nPassword":"%s"}`, skey1, "password2", "password1"))
		req, err = http.NewRequest(http.MethodPut, changePasswordURL, bytes.NewBuffer(payload))
		if err != nil {
			t.Error(err)
		}
		resp, err = client.Do(req)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusAccepted, resp.StatusCode, "should return 202")
	})

	t.Run("ValidCurrentPasswordWith2FA", func(t *testing.T) {
		payload := []byte(fmt.Sprintf(`{"sessionKey":"%s","cPassword":"%s","nPassword":"%s"}`, skey2, "password1", "password2"))
		req, err := http.NewRequest(http.MethodPut, changePasswordURL, bytes.NewBuffer(payload))
		if err != nil {
			t.Error(err)
		}
		resp, err = client.Do(req)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusAccepted, resp.StatusCode, "should return 202")

		// chaneg it back
		payload = []byte(fmt.Sprintf(`{"sessionKey":"%s","cPassword":"%s","nPassword":"%s"}`, skey2, "password2", "password1"))
		req, err = http.NewRequest(http.MethodPut, changePasswordURL, bytes.NewBuffer(payload))
		if err != nil {
			t.Error(err)
		}
		resp, err = client.Do(req)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, http.StatusAccepted, resp.StatusCode, "should return 202")
	})
}
