using Albireo.Base32;
using AspNetCore.Totp;
using CryptSharp.Utility;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using OtpNet;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace dotnetcore
{
    public class Startup
    {

        public static IConfigurationRoot Configuration { get; set; }
        public static DbHelper dbHelper;
        public Startup()
        {
            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            dbHelper = new DbHelper(Configuration.GetConnectionString("DataAccessPostgreSqlProvider"));

        }

        public void Configure(IApplicationBuilder app)
        {
            app.Map("/user", UserHandler);
            app.Map("/login", LoginHandler);
            app.Run(context =>
            {
                return context.Response.WriteAsync("default");
            });
        }

        private static void UserHandler(IApplicationBuilder app)
        {
            app.Map("/password", ChangePasswordHandler);
        }

        private static void ChangePasswordHandler(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                var lcpreq = new ChangePasswordRequest();
                try
                {
                    lcpreq = JsonConvert.DeserializeObject<ChangePasswordRequest>(new StreamReader(context.Request.Body).ReadToEnd());

                    //validate session
                    var validateSessionResult = dbHelper.GetUserAndValidateSession(lcpreq.sessionKey);
                    if (!String.IsNullOrEmpty(validateSessionResult.error))
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        await context.Response.WriteAsync("unable to authenticate, session may have expired: " + validateSessionResult.error);
                        return;
                    }
                    //update session
                    var pwdExpire = DateTime.UtcNow.AddHours(1);
                    var updateSessionResult = dbHelper.UpdateSession(pwdExpire, lcpreq.sessionKey);
                    if (updateSessionResult != "success")
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        await context.Response.WriteAsync("error updating session: " + updateSessionResult);
                        return;
                    }

                    //verify password
                    var inputPasswordBytes = Encoding.UTF8.GetBytes(lcpreq.cPassword);
                    var computedPassworHash = new byte[32];
                    SCrypt.ComputeKey(inputPasswordBytes, validateSessionResult.user.password_salt, 32768, 8, 1, 32, computedPassworHash);
                    var computedPasswordHashString = Encoding.UTF8.GetString(computedPassworHash, 0, computedPassworHash.Length);
                    var userPasswordHashString = Encoding.UTF8.GetString(validateSessionResult.user.password_hash, 0, validateSessionResult.user.password_hash.Length);
                    if (!String.Equals(computedPasswordHashString, userPasswordHashString))
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                        return;
                    }

                    //var inputPasswordBytes = Encoding.UTF8.GetBytes(lreq.password);
                    //var computedPassworHash = new byte[32];
                    //SCrypt.ComputeKey(inputPasswordBytes, userRow.password_salt, 32768, 8, 1, 32, computedPassworHash);
                    //var computedPasswordHashString = Encoding.UTF8.GetString(computedPassworHash, 0, computedPassworHash.Length);
                    //var userPasswordHashString = Encoding.UTF8.GetString(userRow.password_hash, 0, userRow.password_hash.Length);
                    //if (!String.Equals(computedPasswordHashString, userPasswordHashString))
                    //{
                    //    context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    //    return;
                    //}

                    //update password
                    var nPasswordBytes = Encoding.UTF8.GetBytes(lcpreq.nPassword);
                    var nSalt = GetRandomHexNumber(32);
                    var nHash = new byte[32];
                    SCrypt.ComputeKey(nPasswordBytes, Encoding.UTF8.GetBytes(nSalt), 32768, 8, 1, 32, nHash);

                    var updatePasswordResult = dbHelper.UpdatePassword(validateSessionResult.user.uuid, nSalt, nHash);
                    if(updatePasswordResult != "success")
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        await context.Response.WriteAsync("error updating password: " + updatePasswordResult);
                        return;
                    }

                    context.Response.StatusCode = (int)HttpStatusCode.Accepted;
                    

                }
                catch (Exception e)
                {

                }

                await context.Response.WriteAsync("change password");

            });
        }

        private static void LoginHandler(IApplicationBuilder app)
        {
            app.Map("/otp", LoginOtpHandler);
            app.Run(async context =>
            {
                var lreq = new LoginRequest();
                try
                {
                    lreq = JsonConvert.DeserializeObject<LoginRequest>(new StreamReader(context.Request.Body).ReadToEnd());
                    //find user
                    if (!dbHelper.FindUserByName(lreq.userName))
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        await context.Response.WriteAsync("unable to get user");
                        return;
                    }

                    //verify password
                    var userRow = dbHelper.GetUserByName(lreq.userName);
                    var inputPasswordBytes = Encoding.UTF8.GetBytes(lreq.password);
                    var computedPassworHash = new byte[32];
                    SCrypt.ComputeKey(inputPasswordBytes, userRow.password_salt, 32768, 8, 1, 32, computedPassworHash);
                    var computedPasswordHashString = Encoding.UTF8.GetString(computedPassworHash, 0, computedPassworHash.Length);
                    var userPasswordHashString = Encoding.UTF8.GetString(userRow.password_hash, 0, userRow.password_hash.Length);
                    if (!String.Equals(computedPasswordHashString, userPasswordHashString))
                    {
                        context.Response.StatusCode = (int) HttpStatusCode.Unauthorized;
                        return;
                    }

                    //create session key
                    var sKey = GetRandomHexNumber(32);
                    var pwdExpire = DateTime.UtcNow.AddHours(1);
                    
                    if(!dbHelper.CreateSession(userRow.uuid, sKey, pwdExpire, !userRow.opt_enabled))
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        return;
                    }

                    if (userRow.opt_enabled)
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.Accepted;
                    }
                    else
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.OK;
                    }

                    var obj = new LoginResponse
                    {
                        require2fa = userRow.opt_enabled,
                        sessionKey = sKey
                    };

                    var jsonResponse = JsonConvert.SerializeObject(obj);
                    context.Response.Headers.Add("Content-Type", "application/json");
                    await context.Response.WriteAsync(jsonResponse);


                }
                catch (Exception e)
                {
                    context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
                    await context.Response.WriteAsync("unable to parse json: " + e.Message);
                }
            });
        }

        private static void LoginOtpHandler(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                var loreq = new LoginOtpRequest();
                try
                {
                    loreq = JsonConvert.DeserializeObject<LoginOtpRequest>(new StreamReader(context.Request.Body).ReadToEnd());
                    
                    // get session info and opt secret
                    var base32Secret = dbHelper.GetOptSecret(loreq.sessionKey);
                    if (string.IsNullOrEmpty(base32Secret))
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                        return;
                    }
                    //[153 79 244 223 66 132 63 124 162 215]
                    //byte[] secret = { 153, 79, 244, 223, 66, 132, 63, 124, 162, 215};

                    var secret = Base32.Decode(base32Secret);
                    // validate
                    var tfa = new Totp(secret, mode: OtpHashMode.Sha1);
                    //int totpCode = 0;
                    //Int32.TryParse(loreq.otp, out totpCode);

                    // test with own library to generate code
                    //var tfaGenerator = new TotpGenerator();
                    //totpCode = tfaGenerator.Generate(secret);
                    var totpCode = tfa.ComputeTotp();
                    var totpCode2 = tfa.ComputeTotp(DateTime.UtcNow);

                    var window = new VerificationWindow(previous: 1, future: 1);
                    long timeWindowUsed = 0;
                    if (!tfa.VerifyTotp(loreq.otp, out timeWindowUsed, window))
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                        return;
                    }

                    // update sessison
                    var pwdExpire = DateTime.UtcNow.AddHours(1);
                    var tfaExpire = DateTime.UtcNow.AddDays(1);
                    if (!dbHelper.UpdateSession(pwdExpire, tfaExpire, loreq.sessionKey))
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        await context.Response.WriteAsync("error updating session");
                        return;
                    }

                    // send response
                    var obj = new LoginOtpRequest
                    {
                        sessionKey = loreq.sessionKey
                    };

                    var jsonResponse = JsonConvert.SerializeObject(obj);
                    context.Response.Headers.Add("Content-Type", "application/json");
                    await context.Response.WriteAsync(jsonResponse);

                }
                catch (Exception e)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    await context.Response.WriteAsync("unable to parse json: " + e.Message);
                }
            });
        }

        static Random random = new Random();
        private static string GetRandomHexNumber(int digits)
        {
            byte[] buffer = new byte[digits];
            random.NextBytes(buffer);
            string result = String.Concat(buffer.Select(x => x.ToString("x2")).ToArray());
            if (digits % 2 == 0)
                return result;
            return result + random.Next(32).ToString("x");
        }
    }
}