﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using System.Net;

namespace dotnetcore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Server is running...");

            var host = new WebHostBuilder()
                .UseKestrel(
                    options =>
                    {
                        options.Listen(IPAddress.Loopback, 8080);
                    }
                )
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
