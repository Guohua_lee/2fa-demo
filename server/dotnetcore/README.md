Prerequisite: have dotnet 2.0 installed
- Run `dotnet restore` to install dependencies
- Run `dotnet build`
- Run `dotnet run` the server will running at `http://127.0.0.1:800`