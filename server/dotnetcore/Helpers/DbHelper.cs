﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace dotnetcore
{
    public class DbHelper
    {
        private readonly string connString;

        public DbHelper(string connString)
        {
            this.connString = connString;
        }
        public void GetAllUsers()
        {
            using (var conn = new NpgsqlConnection(connString))
            {
                conn.Open();

                // Retrieve all rows
                using (var cmd = new NpgsqlCommand("SELECT * FROM users", conn))
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        string result = reader.GetString(0);
                        Console.WriteLine(result);
                    }
                conn.Close();
            }
        }

        public Boolean FindUserByName(string name)
        {
            var result = false;
            using (var conn = new NpgsqlConnection(connString))
            {
                conn.Open();

                // Retrieve all rows
                using (var cmd = new NpgsqlCommand("SELECT * FROM users where username = @username", conn))
                {
                    cmd.Parameters.AddWithValue("@username", name);
                    using (var reader = cmd.ExecuteReader())
                        if (reader.HasRows)
                            result = true;
                }
                conn.Close();
            }
            return result;
        }

        public UserRow GetUserByName(string name)
        {
            if (this.FindUserByName(name))
            {
                UserRow user = new UserRow();
                using (var conn = new NpgsqlConnection(connString))
                {
                    conn.Open();

                    // Retrieve all rows
                    using (var cmd = new NpgsqlCommand("SELECT * FROM users where username = @username", conn))
                    {
                        cmd.Parameters.AddWithValue("@username", name);
                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                user.uuid = reader.GetGuid(0).ToString();
                                user.username = reader.GetString(1);
                                user.password_salt = (byte[])reader[2];
                                user.password_hash = (byte[])reader[3];
                                user.opt_enabled = reader.GetBoolean(4);
                            }

                        }

                    }
                    conn.Close();
                }
                return user;
            }
            else
            {
                return null;
            }
        }

        public Boolean CreateSession(string userId, string sessionKey, DateTime expire_at, bool fully_logged_in)
        {
            var result = false;
            try
            {
                using (var conn = new NpgsqlConnection(connString))
                {
                    conn.Open();

                    // Retrieve all rows
                    using (var cmd = new NpgsqlCommand("INSERT INTO sessions VALUES (@newGuid, @userId, @sessionKey, @expire_at,  NULL, @fully_logged_in)", conn))
                    {
                        var sessionGuid = Guid.NewGuid();
                        var sessionParam = new NpgsqlParameter("sessionKey", NpgsqlDbType.Bytea);
                        sessionParam.Value = Encoding.UTF8.GetBytes(sessionKey);
                        cmd.Parameters.Add(sessionParam);
                        cmd.Parameters.AddWithValue("@newGuid", sessionGuid);
                        cmd.Parameters.AddWithValue("@userId", Guid.Parse(userId));
                        cmd.Parameters.AddWithValue("@expire_at", expire_at);
                        cmd.Parameters.AddWithValue("@fully_logged_in", fully_logged_in);

                        cmd.ExecuteNonQuery();
                    }
                    conn.Close();
                }
                result = true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error add session: " + e.Message);
            }
            return result;
        }

        public Boolean UpdateSession(DateTime pwdExpire, DateTime tfaExpire, string sessionKey)
        {
            var result = false;
            try
            {
                using (var conn = new NpgsqlConnection(connString))
                {
                    conn.Open();

                    // Retrieve all rows
                    using (var cmd = new NpgsqlCommand(@"UPDATE sessions 
                        SET password_auth_expires_at = @pwdExpire, two_factor_auth_expires_at = @tfaExpire, fully_logged_in = @fullyLoggedIn 
                        WHERE session_key = @sessionKey",
                        conn))
                    {
                        var sessionGuid = Guid.NewGuid();
                        var sessionParam = new NpgsqlParameter("sessionKey", NpgsqlDbType.Bytea);
                        sessionParam.Value = Encoding.UTF8.GetBytes(sessionKey);
                        cmd.Parameters.Add(sessionParam);
                        cmd.Parameters.AddWithValue("@pwdExpire", pwdExpire);
                        cmd.Parameters.AddWithValue("@tfaExpire", tfaExpire);
                        cmd.Parameters.AddWithValue("@fullyLoggedIn", true);
                        cmd.ExecuteNonQuery();
                    }
                    conn.Close();
                }
                result = true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error update session: " + e.Message);
            }
            return result;
        }

        public string UpdateSession(DateTime pwdExpire, string sessionKey)
        {
            var result = "";
            try
            {
                using (var conn = new NpgsqlConnection(connString))
                {
                    conn.Open();

                    // Retrieve all rows
                    using (var cmd = new NpgsqlCommand(@"UPDATE sessions 
                        SET password_auth_expires_at = @pwdExpire 
                        WHERE encode(session_key, 'escape') = @sessionKey",
                        conn))
                    {

                        cmd.Parameters.AddWithValue("@sessionKey", sessionKey);
                        cmd.Parameters.AddWithValue("@pwdExpire", pwdExpire);
                        cmd.ExecuteNonQuery();
                    }
                    conn.Close();
                }
                result = "success";
            }
            catch (Exception e)
            {
                Console.WriteLine("Error update session: " + e.Message);
                result = "Error update session: " + e.Message;
            }
            return result;
        }

        public string GetOptSecret(string sessionKey)
        {
            string secret = string.Empty;
            try
            {
                using (var conn = new NpgsqlConnection(connString))
                {
                    conn.Open();

                    // Retrieve all rows
                    using (var cmd = new NpgsqlCommand(@"SELECT o.secret
			            FROM sessions AS s LEFT JOIN otp AS o ON s.user_id = o.user_id
			            WHERE encode(s.session_key, 'escape') = @sessionKey", conn))
                    {
                        cmd.Parameters.AddWithValue("sessionKey", sessionKey);
                        using (var reader = cmd.ExecuteReader())
                            if (reader.Read())
                                secret = reader.GetString(0);
                    }
                    conn.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return secret;
        }

        public string UpdatePassword(string userId, string nSalt, byte[] nHash)
        {
            var result = string.Empty;
            try
            {
                using (var conn = new NpgsqlConnection(connString))
                {
                    conn.Open();

                    // Retrieve all rows
                    using (var cmd = new NpgsqlCommand(@"UPDATE users SET password_salt = @password_salt, password_hash =@password_hash WHERE user_id =@user_id",
                        conn))
                    {
                       
                        cmd.Parameters.AddWithValue("@password_salt", Encoding.UTF8.GetBytes(nSalt));
                        cmd.Parameters.AddWithValue("@password_hash", nHash);
                        cmd.Parameters.AddWithValue("@user_id", Guid.Parse(userId));
                        cmd.ExecuteNonQuery();
                    }
                    conn.Close();
                }
                result = "success";
            }
            catch (Exception e)
            {
                Console.WriteLine("Error update password: " + e.Message);
                result = e.Message;
            }
            return result;
        }

        public ValidateSessionResult GetUserAndValidateSession(string sessionKey)
        {
            var result = new ValidateSessionResult();
            result.user = new UserRow();
            try
            {
                using (var conn = new NpgsqlConnection(connString))
                {
                    conn.Open();

                    // Retrieve all rows
                    using (var cmd = new NpgsqlCommand(@"SELECT s.password_auth_expires_at, s.two_factor_auth_expires_at, s.fully_logged_in, u.*
			            FROM sessions AS s LEFT JOIN users AS u ON s.user_id = u.user_id
			            WHERE encode(s.session_key, 'escape') = @sessionKey", conn))
                    {
                        cmd.Parameters.AddWithValue("sessionKey", sessionKey);
                        using (var reader = cmd.ExecuteReader())
                            if (reader.Read())
                            {
                                var pExpire = reader.GetDateTime(0);
                                DateTime tfaExpire;
                                try
                                {
                                   tfaExpire = reader.GetDateTime(1);
                                }
                                catch
                                {
                                    tfaExpire = DateTime.UtcNow.AddDays(1);
                                }
                                var loggedIn = reader.GetBoolean(2);
                                result.user.uuid = reader.GetGuid(3).ToString();
                                result.user.username = reader.GetString(4);
                                result.user.password_salt = (byte[])reader[5];
                                result.user.password_hash = (byte[])reader[6];
                                result.user.opt_enabled = reader.GetBoolean(7);

                                if( pExpire < DateTime.UtcNow ||
                                    tfaExpire< DateTime.UtcNow ||
                                    !loggedIn||
                                    String.IsNullOrEmpty(result.user.uuid))
                                {
                                    result.error = "user session has expired";
                                    return result;
                                }

                                if (String.IsNullOrEmpty(result.user.username))
                                {
                                    result.error = "unable to find valid user";
                                    return result;
                                }
                            }
                    }
                    conn.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result.error = e.Message;
            }
            return result;
        }
    }
}
