﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnetcore
{
    public class UserRow
    {
        public string uuid { get; set; }
        public string username { get; set; }
        public byte[] password_salt { get; set; }
        public byte[] password_hash { get; set; }
        public Boolean opt_enabled { get; set; }
    }
}
