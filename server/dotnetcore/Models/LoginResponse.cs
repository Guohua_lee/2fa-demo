﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnetcore
{
    public class LoginResponse
    {
        public bool require2fa { get; set; }
        public string sessionKey { get; set; }
    }
}
