﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnetcore
{
    public class LoginOtpRequest
    {
        public string sessionKey { get; set; }
        public string otp { get; set; }
    }
}
