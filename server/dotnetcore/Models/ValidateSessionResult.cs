﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnetcore
{
    public class ValidateSessionResult
    {
        public UserRow user { get; set; }
        public string error { get; set; }
    }
}
