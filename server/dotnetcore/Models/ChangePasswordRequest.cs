﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnetcore
{
    public class ChangePasswordRequest
    {
        public string sessionKey { get; set; }
        public string cPassword { get; set; }
        public string nPassword { get; set; }
    }
}
