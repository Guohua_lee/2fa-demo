--
-- DROP TABLES
--
DROP TABLE IF EXISTS sessions;
DROP TABLE IF EXISTS otp_recovery;
DROP TABLE IF EXISTS otp;
DROP TABLE IF EXISTS users;

--
-- CREATE TABLES
--
CREATE TABLE users (
  user_id UUID PRIMARY KEY,
  username VARCHAR(256) NOT NULL,
  password_salt BYTEA NOT NULL,
  password_hash BYTEA NOT NULL,
  otp_enabled BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE otp (
  otp_id UUID PRIMARY KEY,
  user_id UUID NOT NULL REFERENCES users(user_id),
  secret VARCHAR(256) NOT NULL
);

CREATE TABLE otp_recovery (
  otpr_id UUID PRIMARY KEY,
  user_id UUID NOT NULL REFERENCES users(user_id),
  used BOOLEAN NOT NULL DEFAULT FALSE,
  recovery_code VARCHAR(256) NOT NULL
);

CREATE TABLE sessions (
  session_id UUID PRIMARY KEY,
  user_id UUID NOT NULL REFERENCES users(user_id),
  session_key BYTEA NOT NULL,
  password_auth_expires_at TIMESTAMP NOT NULL,
  two_factor_auth_expires_at TIMESTAMP NULL,
  fully_logged_in BOOLEAN NOT NULL DEFAULT FALSE
);

--
-- CREATE USER
--
-- user1 / password1
INSERT INTO users VALUES (
  '32a9b04b-d4aa-49d4-80e6-d7f481904742',
  'user1',
  E'\\xb69d1ef795e7bd9943e187fc6c307675a4e0356e059b0616aeed78f8b78a0426',
  E'\\xa25e38562160698a9749b014d0ca2ca458ffe0135e4061bb033c11e1ea4f4c16',
  FALSE
);
-- user2 / password1
INSERT INTO users VALUES (
  '872f4f60-f13a-4091-8cab-1ff17e8e7b7b',
  'user2',
  E'\\xb69d1ef795e7bd9943e187fc6c307675a4e0356e059b0616aeed78f8b78a0426',
  E'\\xa25e38562160698a9749b014d0ca2ca458ffe0135e4061bb033c11e1ea4f4c16',
  TRUE
);

-- user2 otp secret
INSERT INTO otp VALUES (
  '641e1458-68b7-4738-884d-9f050502db98',
  '872f4f60-f13a-4091-8cab-1ff17e8e7b7b',
  'TFH7JX2CQQ7XZIWX'
);